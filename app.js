const init = () => {
  const usernameTextInput = document.getElementById('username');
  const resultPanel = document.getElementById('result');

  const getRepos = username =>
    fetch(`https://api.github.com/users/${username}/repos`).then(res =>
      res.json()
    );

  const getStargazers = fullName =>
    fetch(`https://api.github.com/repos/${fullName}/stargazers`).then(res =>
      res.json()
    );

  const renderStargazers = (id, stargazers) => {
    const button = document.getElementById(`loadStarsFor${id}Button`);
    const repoLi = document.getElementById(id);
    button.parentNode.removeChild(button);

    const ul = document.createElement('ul');

    for (let stargazer of stargazers) {
      const li = document.createElement('li');
      li.innerHTML = stargazer.login;
      ul.append(li);
    }

    repoLi.append(ul);
  };

  const renderRepoList = repos => {
    resultPanel.innerHTML = '';

    for (let repo of repos) {
      const li = document.createElement('li');
      li.append(repo.name);
      li.append(' ');
      li.id = repo.id;

      const button = document.createElement('button');
      button.id = `loadStarsFor${repo.id}Button`;
      button.innerHTML = 'Load stargazers';

      button.addEventListener('click', () => {
        fetchStargazers(repo);
      });

      li.append(button);

      resultPanel.append(li);
    }
  };

  const fetchStargazers = async repo => {
    const button = document.getElementById(`loadStarsFor${repo.id}Button`);
    button.innerHTML = 'Loading...';
    const stargazers = await getStargazers(repo.full_name);
    renderStargazers(repo.id, stargazers);
  };

  const fetchRepos = async username => {
    resultPanel.innerHTML = `Loading repos for ${username}`;
    const repos = await getRepos(username);
    renderRepoList(repos);
  };

  usernameTextInput.addEventListener('keyup', e => {
    if (e.keyCode === 13) {
      fetchRepos(usernameTextInput.value);
    }
  });
};

window.onload = init;
